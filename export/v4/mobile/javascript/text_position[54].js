﻿positionForPages[54]={page:54,positions:[{word:"54",left:0.057031,top:0.049830,width:0.024124,height:0.027460},{word:"BMW",left:0.122213,top:0.049830,width:0.050919,height:0.027460},{word:"MOTORRAD",left:0.176761,top:0.049830,width:0.112187,height:0.027460},{word:"PERFORMANCE",left:0.292575,top:0.049830,width:0.146010,height:0.027460},{word:"ACADEMY",left:0.442213,top:0.049830,width:0.095618,height:0.027460},{word:"/",left:0.541460,top:0.049830,width:0.007975,height:0.027460},{word:"RIDER",left:0.553064,top:0.049830,width:0.057437,height:0.027460},{word:"EQUIPMENT",left:0.614128,top:0.049830,width:0.114393,height:0.027460},{word:"BMW",left:0.055915,top:0.147867,width:0.118498,height:0.064073},{word:"MOTORRAD",left:0.185415,top:0.147867,width:0.260738,height:0.064073},{word:"PERFORMANCE",left:0.055915,top:0.194390,width:0.339125,height:0.064073},{word:"ACADEMY",left:0.404124,top:0.194390,width:0.222188,height:0.064073},{word:"DATES",left:0.056696,top:0.293249,width:0.080731,height:0.036613},{word:"(2022)",left:0.142265,top:0.293249,width:0.077241,height:0.036613},{word:"25",left:0.079498,top:0.353896,width:0.027947,height:0.031816},{word:"16",left:0.166748,top:0.353896,width:0.027098,height:0.031816},{word:"07",left:0.251441,top:0.353896,width:0.027803,height:0.031816},{word:"April",left:0.078781,top:0.379636,width:0.029381,height:0.018181},{word:"May",left:0.167447,top:0.379636,width:0.025707,height:0.018181},{word:"June",left:0.250840,top:0.379636,width:0.029007,height:0.018181},{word:"18",left:0.079838,top:0.434253,width:0.027274,height:0.031816},{word:"30",left:0.166166,top:0.434253,width:0.028266,height:0.031816},{word:"05",left:0.251248,top:0.434253,width:0.028200,height:0.031816},{word:"July",left:0.081262,top:0.459993,width:0.024426,height:0.018181},{word:"August",left:0.158781,top:0.459993,width:0.043036,height:0.018181},{word:"September",left:0.231579,top:0.459993,width:0.067525,height:0.018181},{word:"WHAT",left:0.058032,top:0.510359,width:0.076303,height:0.036613},{word:"DO",left:0.139173,top:0.510359,width:0.039158,height:0.036613},{word:"I",left:0.183170,top:0.510359,width:0.007812,height:0.036613},{word:"NEED",left:0.195819,top:0.510359,width:0.070117,height:0.036613},{word:"\u2022",left:0.058036,top:0.555779,width:0.004964,height:0.017828},{word:"\u2022",left:0.058036,top:0.571288,width:0.004964,height:0.017828},{word:"Full",left:0.069199,top:0.555779,width:0.019650,height:0.017828},{word:"unrestricted",left:0.092729,top:0.555779,width:0.070644,height:0.017828},{word:"UK",left:0.167253,top:0.555779,width:0.017455,height:0.017828},{word:"motorcycle",left:0.188589,top:0.555779,width:0.064712,height:0.017828},{word:"licence",left:0.257181,top:0.555779,width:0.040258,height:0.017828},{word:"\u2022",left:0.058036,top:0.555779,width:0.004964,height:0.017828},{word:"\u2022",left:0.058036,top:0.571288,width:0.004964,height:0.017828},{word:"One-piece",left:0.069199,top:0.571288,width:0.060877,height:0.017828},{word:"or",left:0.211956,top:0.555779,width:0.012235,height:0.017828},{word:"or",left:0.133956,top:0.571288,width:0.012192,height:0.017828},{word:"or",left:0.206217,top:0.586798,width:0.012192,height:0.017828},{word:"two-piece",left:0.150029,top:0.571288,width:0.059167,height:0.017828},{word:"zip-together",left:0.213076,top:0.571288,width:0.071779,height:0.017828},{word:"leathers",left:0.069199,top:0.586798,width:0.046163,height:0.017828},{word:"a",left:0.079149,top:0.586798,width:0.006702,height:0.017828},{word:"a",left:0.121939,top:0.586798,width:0.006652,height:0.017828},{word:"a",left:0.140138,top:0.586798,width:0.006792,height:0.017828},{word:"a",left:0.235509,top:0.586798,width:0.006757,height:0.017828},{word:"a",left:0.109120,top:0.602308,width:0.006791,height:0.017828},{word:"back",left:0.132472,top:0.586798,width:0.027566,height:0.017828},{word:"protector",left:0.163918,top:0.586798,width:0.054492,height:0.017828},{word:"is",left:0.222290,top:0.586798,width:0.009339,height:0.017828},{word:"also",left:0.235509,top:0.586798,width:0.023302,height:0.017828},{word:"encouraged",left:0.069199,top:0.602308,width:0.069065,height:0.017828},{word:"\u2022",left:0.058036,top:0.617817,width:0.004964,height:0.017828},{word:"Full-face",left:0.069199,top:0.617817,width:0.048682,height:0.017828},{word:"motorcycle",left:0.121761,top:0.617817,width:0.064712,height:0.017828},{word:"helmet",left:0.190354,top:0.617817,width:0.040043,height:0.017828},{word:"with",left:0.234277,top:0.617817,width:0.025746,height:0.017828},{word:"ACU",left:0.263904,top:0.617817,width:0.026179,height:0.017828},{word:"approval",left:0.069199,top:0.633327,width:0.049953,height:0.017828},{word:"(gold",left:0.123032,top:0.633327,width:0.029961,height:0.017828},{word:"sticker)",left:0.156874,top:0.633327,width:0.042895,height:0.017828},{word:"\u2022",left:0.058036,top:0.648836,width:0.004964,height:0.017828},{word:"Motorcycle",left:0.069199,top:0.648836,width:0.064469,height:0.017828},{word:"gloves",left:0.137548,top:0.648836,width:0.037084,height:0.017828},{word:"that",left:0.178513,top:0.648836,width:0.022476,height:0.017828},{word:"must",left:0.204869,top:0.648836,width:0.029213,height:0.017828},{word:"go",left:0.127175,top:0.633327,width:0.015288,height:0.017828},{word:"go",left:0.237962,top:0.648836,width:0.015140,height:0.017828},{word:"over",left:0.256983,top:0.648836,width:0.025133,height:0.017828},{word:"the",left:0.285997,top:0.648836,width:0.018523,height:0.017828},{word:"the",left:0.156956,top:0.664346,width:0.018624,height:0.017828},{word:"the",left:0.299993,top:0.664346,width:0.018624,height:0.017828},{word:"cuff",left:0.069199,top:0.664346,width:0.022461,height:0.017828},{word:"of",left:0.095540,top:0.664346,width:0.011716,height:0.017828},{word:"your",left:0.111136,top:0.664346,width:0.025287,height:0.017828},{word:"leathers",left:0.140303,top:0.664346,width:0.046383,height:0.017828},{word:"and",left:0.193074,top:0.664346,width:0.021484,height:0.017828},{word:"calf-length",left:0.218439,top:0.664346,width:0.061023,height:0.017828},{word:"leather",left:0.140303,top:0.664346,width:0.040160,height:0.017828},{word:"leather",left:0.283342,top:0.664346,width:0.039887,height:0.017828},{word:"motorcycle",left:0.069199,top:0.679856,width:0.064712,height:0.017828},{word:"boots",left:0.137791,top:0.679856,width:0.033370,height:0.017828},{word:"Find",left:0.057031,top:0.920752,width:0.039298,height:0.027271},{word:"out",left:0.100544,top:0.920752,width:0.029730,height:0.027271},{word:"more",left:0.134488,top:0.920752,width:0.046941,height:0.027271},{word:"at",left:0.185643,top:0.920752,width:0.017576,height:0.027271},{word:"worldofbmw",left:0.207434,top:0.920752,width:0.113111,height:0.027271},{word:"com",left:0.325065,top:0.920752,width:0.038351,height:0.027271},{word:"or",left:0.151724,top:0.920752,width:0.018914,height:0.027271},{word:"or",left:0.222965,top:0.920752,width:0.018801,height:0.027271},{word:"or",left:0.367631,top:0.920752,width:0.019007,height:0.027271},{word:"call",left:0.390852,top:0.920752,width:0.030193,height:0.027271},{word:"08000",left:0.425259,top:0.920752,width:0.061094,height:0.027271},{word:"131",left:0.490568,top:0.920752,width:0.033785,height:0.027271},{word:"282",left:0.528568,top:0.920752,width:0.035740,height:0.027271}]};